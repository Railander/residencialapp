var express = require('express'); 
var app = express();
var path = require('path');
var gpio = require('rpi-gpio');

//Definicao dos pinos que vamos utilizar na raspberry
gpio.setup(12, gpio.DIR_OUT);
gpio.setup(24, gpio.DIR_OUT);


app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

console.log(path.join(__dirname, 'public'));

app.get('/', function(req, res){ 
 	res.render('index',{status:"Selecione um refletor para acender"});
});

// Configuracao PINO 12
app.post('/led/on', function(req, res){
gpio.write(12, true, function(err) {
        if (err) throw err;
        console.log('Written True to pin');
	console.log(path.join(__dirname, 'public'));
	return res.render('index', {status: "Lampada 1 esta ligada"});
    });

});

app.post('/led/off', function(req, res){
gpio.write(12, false, function(err) {
        if (err) throw err;
        console.log('Written False to pin');
        console.log(path.join(__dirname, 'public'));
        return res.render('index',{status: "Lampada 1 esta desligada"});
    });
});

//configuracao PINO 24

app.post('/led2/on', function(req, res){
gpio.write(24, true, function(err) {
        if (err) throw err;
        console.log('Written True to pin');
        console.log(path.join(__dirname, 'public'));
        return res.render('index', {status: "Lampada 2 esta ligada"});
    });
});

app.post('/led2/off', function(req, res){
gpio.write(24, false, function(err) {
        if (err) throw err;
        console.log('Written False to pin');
        console.log(path.join(__dirname, 'public'));
        return res.render('index',{status: "Lampada 2 esta desligada"});
    });
});


//PORTA UTILIZADA PELO SERVIDOR NODE.
app.listen(80, function () {
  console.log('Simple LED Control Server Started on Port: 80!')
})
